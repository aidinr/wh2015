/* Victoria v1.0 | Copyright 2014 Limitless LLC */
jQuery(document).ready(function($) {
   'use strict';

	var homeVideoMuted = true;
	var windowHeight = $(window).height();
	var windowWidth = $(window).width();

	// Navigation Menu
    $(".header .menu").click(function(e){
    	if ($('.header .navigation').is(":hidden")) {
    		$('.header').stop().animate({ height: windowHeight }, 'slow');
    		$('.header .logo').stop().animate({ top: windowHeight - 55 }, 'fast');
    		$('.header .menu').stop().animate({ top: windowHeight - 45 }, 'fast');
			$('.header .navigation').slideDown("fast","easeInQuart");
			var md = windowHeight - $('.header .navigation ul').height();
    		$('.header .navigation ul').stop().animate({ marginTop: md / 2 }, 'fast');
		} else {
    		$('.header .logo').stop().animate({ top: 15 }, 'fast');
    		$('.header .menu').stop().animate({ top: 27 }, 'fast');
			$('.header .navigation').slideUp("fast","easeOutQuart");
    		$('.header').stop().animate({ height: 70 }, 'slow');
		}
    });

	$(".navigation li").click(function(e){
		var type = $(this).attr("data-type");
		if(type==="in") {

			var name = $(this).attr("data-url");
			$('.navigation li[data-url="' + name + '"]').addClass('active', {duration:300});
			$('.navigation li[data-url="' + name + '"]').siblings().removeClass('active', {duration:300});
			
	    	if (!$('.header .navigation').is(":hidden")) {
				$('.header .logo').stop().animate({ top: 15 }, 'fast');
	    		$('.header .menu').stop().animate({ top: 27 }, 'fast');
	    		if (!$('.header .menu').is(":hidden")) $('.header .navigation').slideUp("fast","easeOutQuart");
	    		$('.header').stop().animate({ height: '80px' }, 'fast', function() {
	    			$('html,body').stop().animate({scrollTop: $("section."+name).position().top - 60}, 'slow');
				});
			}
		} else {
			var url = $(this).attr("data-url");
			window.location = url;
		}
	});
});

$(window).load(function() {
	fixSizes();
	var windowHeight = $(window).height();
	var windowWidth = $(window).width();
	if($('section.home').length && $(this).scrollTop() < 180) {
		$('.header').stop().animate({ backgroundColor: 'rgba(17,17,17,0)' }, 'fast');
	} else {
		$('.header').stop().animate({ backgroundColor: 'rgba(17,17,17,1)' }, 'fast');
	}
	var sectionOffset = '15%';
	$("section").waypoint({
		handler: function(event, direction) {
			var name=$(this).attr("id");
			if (direction === "up")  name = $(this).prev().attr("id");
			if (direction === "up")  sectionOffset = '30%';
			$('.navigation li[data-url="' + name + '"]').addClass('active', {duration:300});
			$('.navigation li[data-url="' + name + '"]').siblings().removeClass('active', {duration:300});
	  	},
		offset: sectionOffset
	});
	
	// $(".loader").delay(1000).fadeOut('slow');
	
	//Animations
	setTimeout(function(){$('.header').addClass('animated fadeInDown')},1300);
	setTimeout(function(){$('#home .content').addClass('animated fadeInDown')},1600);

    $('#about').waypoint(function() {
        setTimeout(function(){$('#about .story').addClass('animated fadeInDown')},0);
    }, { offset: '50' });

    $('#team').waypoint(function() {
        setTimeout(function(){$('#team .member').addClass('animated fadeInDown')},0);
    }, { offset: '50' });

    $('#quotes').waypoint(function() {
        setTimeout(function(){$('#quotes .slider').addClass('animated fadeInLeft')},0);
    }, { offset: '50' });

    $('#services').waypoint(function() {
        setTimeout(function(){$('#services .service').addClass('animated fadeInLeft')},0);
    }, { offset: '50' });

    $('#projects').waypoint(function() {
        setTimeout(function(){$('#projects .project').addClass('animated fadeInUp')},0);
    }, { offset: '50' });

    $('#contact').waypoint(function() {
        setTimeout(function(){$('#contact .form').addClass('animated fadeInUp')},0);
        setTimeout(function(){$('#contact .info').addClass('animated fadeInDown')},0);
    }, { offset: '50' });
    //Animations
});
$(window).resize(function() {
	fixSizes();
});
function fixSizes() {
	var windowHeight = $(window).height();
	var windowWidth = $(window).width();
	$(".fullscreen").css('height', windowHeight);
	var rat = windowWidth / windowHeight;
	if (rat > (16/9)) {
		var v = windowWidth * (16/9);
		$(".home video").css('width', windowWidth);
		$(".home video").css('height', v);
		var vc = ($(".home video").height() - windowHeight) / 2;
		$(".home video").css('margin-top', '-'+vc+'px');
		$(".home video").css('margin-left', '0px');
	} else {
		var v = windowHeight * (16/9);
		$(".home video").css('height', windowHeight);
		$(".home video").css('width', v);
		var vc = ($(".home video").width() - windowWidth) / 2;
		$(".home video").css('margin-top', '0px');
		$(".home video").css('margin-left', '-'+vc+'px');
	}
	$(".project").each(function() {
		if ( windowWidth > 960 ) { 
			$(this).css('width', (windowWidth / 4));
			$(this).css('height', (windowWidth / 4));
		} else if ( windowWidth > 767) { 
			$(this).css('width', (windowWidth / 3));
			$(this).css('height', (windowWidth / 3));
		} else if ( windowWidth > 480) { 
			$(this).css('width', (windowWidth / 2));
			$(this).css('height', (windowWidth / 2));
		} else {
			$(this).css('width', windowWidth);
			$(this).css('height', windowWidth);
		}
		$(this).find(".content").css('margin-top', (($(this).height() - $(this).find(".content").height()) / 2));
	});
	$(".vertical-center").each(function() {
		$(this).css('margin-top', ($(this).parent().height() - $(this).height()) / 2);
	});
	var z = (windowHeight - $(".home .flex-control-nav").height()) / 2;
	$(".home .flex-control-nav").css('top', z);
	$(".home .flex-prev").css('top', z - 60);
	$(".home .flex-next").css('top', z + $(".home .flex-control-nav").height() + 55);

	var t = $(".services .filter").width() - ($(".services .filter li").length * 40);
	var p = t / $(".services .filter li").length;
	$(".services .filter li").each(function() {
		$(this).css('margin-right', p/2);
		$(this).css('margin-left', p/2);
	});
	loadServices();
}
function loadServices() {
	if($(".services .filter li").length) {
		$('.services .filter li:eq(0)').trigger( "click" );
	}
}