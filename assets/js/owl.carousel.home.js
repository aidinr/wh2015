// events jquery carousel

$(document).ready(function() {
	$("#owl-demo").owlCarousel({
	    jsonPath : '/assets/js/json/eventsdata.json',
	    jsonSuccess : customDataSuccess
	});

	function customDataSuccess(data) {
	    var content = "";
	    for(var i in data["items"]) {

	        var img = data["items"][i].img;
	        var alt = data["items"][i].alt;
	        var title = data["items"][i].title;
	        var description = data["items"][i].description;

	        var eventstart = data["items"][i].eventstart;
	        var eventend = data["items"][i].eventend;
	        var location = data["items"][i].location;
	        
	        content += "<div class='items-wrapper'>" + "<img src=\"" +img+ "\" alt=\"" +alt+ "\">" + "<h2>" + title + "</h2>" + "<p>" + description + "</p>" + "</div>"
	    }
	    $("#owl-demo").html(content);
	}
});

// $(document).ready(function() {
//     var currentMonth, currentYear;
//     var currentYear = (new Date().getYear() -100) + 2000;
//     var url = 'http://demo.idamcloud.com/finder8/GetEventListByYear_JSONP.aspx';
//     url = url + '?year=' + currentYear + '&width=260&height=260';
//     $.ajax({
//         type: 'GET',
//         url: url,
//         async: false,
//         jsonpCallback: 'jsonp_event_list_items_by_year',
//         contentType: 'application/json',
//         dataType: 'jsonp',
//         success: function(result) {
//             var
//                 theList = [];
//             theList = myFilter(result.rows, 'event_date', currentMonth);
//             loopData(theList, 'value');      
//             $('#owl-demo').owlCarousel();
//         },
//         error: function(e) {
//             console.log('error');
//         }
//     });
//     function myFilter (items, attr, value) {
//         var dataList = [];
//         for (var item in items) {
//             var 
//                 dt = new Date(items[item].value[attr]),
//             dtMonth = dt.getMonth(),
//             check = checkRecurringEvent(items[item].value.UDFs),
//             activeEvent = checkPullEvent(items[item].value);  
//                   dataList.push(items[item]); 
//         }
//         return dataList;
//     }
//     function checkRecurringEvent (items) {
        
//         for (var item in items) {
//             if (items[item].name == 'Recurring Event') {
//                 if (items[item].value == '1') {
//                     return true;
//                 }
//             }
//         }
//         return false;
//     }
//     function checkPullEvent(item){return true;}
//     function checkTodayEvents() {
//         if ($('.pickadate-enabled').hasClass('pickadate-today')) {
//             $('ul.box li').each(function() {                        
//                 var 
//                     ourActiveDay = $('.pickadate-today').text(),
//                 months = [ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ],
//                 getTheMonth = months[$scope.getMonth - 1],
//                 whatDateActive = getTheMonth + ' ' + ourActiveDay + ' ' + $scope.selectedYear,
//                 whatDayActive = new Date(whatDateActive).getDay(),
//                 dayActiveTime = new Date(whatDateActive).getTime(),
//                 weekdays = ["sunday", "monday", "tuesday", "wednesday", "thursday", "friday", "saturday"],
//                 activeDayName = weekdays[whatDayActive],
//                 activeList = [],
//                 eventRuns = $(this).attr('data-day'),
//                 eventRunsDay = new Date(eventRuns).getDay(),
//                 pullOurData = $(this).attr('data-pull'),
//                 pullTime = new Date(pullOurData).getTime();

//                 if (dayActiveTime > pullTime) {
//                     activeList.push(whatDayActive);
//                     $(this).addClass('inactive');  
//                 } 
//             });
//         } else {
//             $(this).removeClass('inactive');
//             emptyMsgShow ();
//         }
//         function emptyMsgShow () {
//             $('ul.box li').each(function() { 
//                 if ($(this).hasClass('inactive')) {
//                     $('.empty-msg').slideDown(10);
//                 } else {
//                     $('.empty-msg').slideUp(10);
//                 }
//             });
//         }
//     }
//     function loopData(items, attr) {
//         $.each(items, function () {
//             $('#owl-demo').prepend(buildEvent(this[attr]));
//         });   
//     }
//     function buildEvent(obj) {
//         var
//             tmp = '';
//         tmp = '<div class="item owl-item">';
//         tmp = '<div class="items-wrapper">';
//         tmp += '<div class="img-container">';
//         if(!!obj.Assets) {
//             if(!!obj.Assets[0].ViewURL) {
//                 tmp += '<div class="obj-tag"><object data="' + obj.Assets[0].ViewURL +'"><img scr="' + obj.Assets[0].ViewURL +'"></object></div>';
//             }
//         } else {
//             tmp += '<img src="/assets/img/events/images/event_default.jpg">';
//         }
//         tmp += '</div>';
//         tmp += '<br>';
        
//         tmp += '<h2><a href="/events"><p>' + obj.headline + '</p></a></h2>';
//          if(!!obj.content) {
//             tmp += '<p>' + obj.content + '</p>';
//         } else {
//             tmp += '<p>No content available.</p>';
//         }

//         tmp += '<p><span class="swipe">Please swipe to see more events.</span></p>';

//         tmp += '</div>';
//         tmp += '</div>';
//         return tmp;
//     }
// });
